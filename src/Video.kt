data class Video(
    val id: String,
    val kanal: String,
    var nazov: String,
    var dlzka: Int,
    var datum: String,
    var cas: String,
    private var _odSekundy: Int,
    private var _poSekundu: Int,
    var tagy: String?
) {

    var odSekundy
        get() = _odSekundy
        set(value) {
            if (value >= 0)
                _odSekundy = value
            else
                _odSekundy = 0
        }

    var poSekundu
        get() = _poSekundu
        set(value) {
            _poSekundu = if (value <= dlzka)
                value
            else
                dlzka
        }

    init {
        odSekundy = _odSekundy
        poSekundu = _poSekundu
    }

    constructor(
        id: String,
        kanal: String,
        nazov: String,
        dlzka: Int,
        datum: String,
        cas: String,
        odSekundy: Int,
        poSekundu: Int
    ) : this(id, kanal, nazov, dlzka, datum, cas, odSekundy, poSekundu, null)

    fun toFileString() =
        "$id\t$kanal\t$nazov\t$dlzka\t$datum\t$cas\t$odSekundy\t$poSekundu${if (tagy != null) "\t$tagy" else ""}"

    fun vytvorOdkaz() = "https://youtu.be/$id${if (odSekundy > 0) "?t=$odSekundy" else ""}"

    companion object {
        @JvmStatic
        fun zoStringu(popis: String): Video {
            val stringVideo = popis.split("\t")
            return if (stringVideo.size == 9) {
                Video(
                    stringVideo[0],
                    stringVideo[1],
                    stringVideo[2],
                    stringVideo[3].toInt(),
                    stringVideo[4],
                    stringVideo[5],
                    stringVideo[6].toInt(),
                    stringVideo[7].toInt(),
                    stringVideo[8]
                )
            } else {
                Video(
                    stringVideo[0],
                    stringVideo[1],
                    stringVideo[2],
                    stringVideo[3].toInt(),
                    stringVideo[4],
                    stringVideo[5],
                    stringVideo[6].toInt(),
                    stringVideo[7].toInt()
                )
            }
        }
    }
}