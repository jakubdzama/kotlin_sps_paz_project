class CasPozretiaKomparator : Comparator<Video> {
    override fun compare(o1: Video, o2: Video): Int {
        val h1 = o1.cas.split(":")[0].toInt()
        val m1 = o1.cas.split(":")[1].toInt()
        val s1 = o1.cas.split(":")[2].toInt()
        val h2 = o2.cas.split(":")[0].toInt()
        val m2 = o2.cas.split(":")[1].toInt()
        val s2 = o2.cas.split(":")[2].toInt()
        return when {
            h1.compareTo(h2) != 0 -> h1.compareTo(h2)
            m1.compareTo(m2) != 0 -> m1.compareTo(m2)
            else -> s1.compareTo(s2)
        }
    }

}