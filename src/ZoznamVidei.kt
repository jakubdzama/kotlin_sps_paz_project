import java.io.File
import kotlin.collections.HashMap
import kotlin.math.max
import kotlin.math.min
import kotlin.text.StringBuilder

class ZoznamVidei {

    var videa: MutableList<Video> = ArrayList()

    constructor()

    constructor(videa: MutableList<Video>) {
        this.videa = videa
    }

    fun pridaj(video: Video) = videa.add(video)

    fun uloz(nazovSuboru: String) {
        val builder = StringBuilder()
        for (video in videa) {
            builder.append(video.toFileString())
            builder.append("\n")
        }
        builder.deleteLastChar()
//        builder.setLength(max(builder.length - 1, 0))
        File(nazovSuboru).writeText(builder.toString())
    }

    fun StringBuilder.deleteLastChar() {
        setLength(max(length, 0))
    }

    fun vratCasPozerania(datum: String) =
        videa.filter { video -> video.datum == datum }
            .map { video -> video.poSekundu - video.odSekundy }
            .sum()

    fun najdiNajdlhsieVideo() = videa.maxBy { video -> video.dlzka }

    fun vratIdVidei() = videa.map { video -> video.id }.distinct()

    fun vratDlhsiePozretia(percentoPozretia: Int): ZoznamVidei {
        val noveVidea =
            videa.filter { video -> (video.poSekundu - video.odSekundy) / video.dlzka.toDouble() >= percentoPozretia / 100.0 }
        return ZoznamVidei(noveVidea.toMutableList())
    }

    fun vratVideaSTagom(tag: String) =
        videa.filter { video -> video.tagy != null && (video.tagy.toString()).matches(Regex(".*$tag.*")) }
            .map { video -> video.id }
            .toSet()

    fun najdiNajoblubenejsiKanal(): String {
        var map = HashMap<String, Int>()
        for (video in videa) {
            map.merge(video.kanal, video.poSekundu - video.odSekundy, Int::plus)
        }
        val max = map.maxBy { entry -> entry.value }
        return max!!.key
    }

    fun zistiPocetPremenovanych(): Int {
        val premenovane = HashSet<String>()
        val map = HashMap<String, String>()
        for (video in videa) {
            if (map.containsKey(video.id)) {
                if (map[video.id] != video.nazov) {
                    premenovane.add(video.id)
                }
            } else {
                map[video.id] = video.nazov
            }
        }
        return premenovane.size
    }

    fun zistiPozretieVidea(id: String): Int {
        val videaWithId = videa.filter { video -> video.id == id }
        var minSek = Int.MAX_VALUE
        var maxSek = Int.MIN_VALUE
        for (video in videaWithId) {
            minSek = min(minSek, video.odSekundy)
            maxSek = max(maxSek, video.poSekundu)
        }
        return if (videaWithId.isEmpty()) 0 else maxSek - minSek
    }

    fun vratIdNajoblubenejsiehoVidea(): String? {
        val map = HashMap<String, HashSet<String>>()
        for (video in videa) {
            if (map.containsKey(video.id)) {
                map[video.id]!!.add(video.datum)
            } else {
                map[video.id] = HashSet()
                map[video.id]!!.add(video.datum)
            }
        }

        val maxEntry = map.maxBy { entry -> entry.value.size }
        return maxEntry?.key
    }

    fun vratHistogramPozerania(): IntArray {
        val result = IntArray(24)
        for (video in videa) {
            var dlzkaPozerania = video.poSekundu - video.odSekundy
            val casString = video.cas.split(":")
            var hour = casString[0].toInt()
            val minute = casString[1].toInt()
            val second = casString[2].toInt()
            var dlzka = min(dlzkaPozerania, 3600 - (minute*60 + second))
            result[hour++] += dlzka
            dlzkaPozerania -= dlzka
            while (dlzkaPozerania > 0) {
                dlzka = min(3600, dlzkaPozerania)
                result[hour++] += dlzka
                dlzkaPozerania -= dlzka
            }
        }
        return result
    }

    fun usporiadajChronologicky() {
        videa.sortWith(CasPozretiaKomparator())
    }

    override fun toString(): String {
        return "ZoznamVidei(videa=$videa)"
    }

    companion object {
        @JvmStatic
        fun zoSuboru(nazovSuboru: String): ZoznamVidei {
            val zoznamVidei = ZoznamVidei()
            File(nazovSuboru).forEachLine { zoznamVidei.pridaj(Video.zoStringu(it)) }
            return zoznamVidei
        }
    }


}